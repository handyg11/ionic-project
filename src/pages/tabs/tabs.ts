import { Component } from '@angular/core';

import { LibraryPage } from '../library/library';
import { QuotesPage } from '../quotes/quotes';
import { FavoritesPage } from '../favorites/favorites';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {


  tab1Root = LibraryPage;
  tab2Root = FavoritesPage;

  constructor() {

  }
}
